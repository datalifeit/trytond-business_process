# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.modules.business_process.process import BusinessProcessMixin


__all__ = ['ProductCategoryLocation', 'Category']


class ProductCategoryLocation(BusinessProcessMixin, metaclass=PoolMeta):
    __name__ = 'stock.product.category.location'

    @classmethod
    def _compute_sequence_where(cls, table, record):
        res = super()._compute_sequence_where(table, record)
        return res & (table.process == record['process'])

    def _get_unique_key(self):
        res = super(ProductCategoryLocation, self)._get_unique_key()
        return res + (self.process.id if self.process else None, )


class Category(metaclass=PoolMeta):
    __name__ = 'product.category'

    def _get_default_location_domain(self, **kwargs):
        res = super(Category, self)._get_default_location_domain(**kwargs)
        process_ids = [None]
        if kwargs.get('process', None):
            process_ids.append(kwargs['process'].id)
        res.append(('process', 'in', process_ids))
        return res

    @classmethod
    def _get_default_location_order(cls):
        res = super(Category, cls)._get_default_location_order()
        return [('process', 'DESC')] + res
