# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, Model
from trytond.pool import PoolMeta
from trytond.transaction import Transaction

__all__ = ['Process', 'BusinessProcessMixin']


class Process(ModelSQL, ModelView):
    """Business process"""
    __name__ = 'business.process'

    name = fields.Char('Name', required=True, translate=True)


class BusinessProcessMixin(object):

    process = fields.Many2One('business.process', 'Process',
                              ondelete='RESTRICT',
                              select=True)

    @classmethod
    def default_process(cls):
        res_id = Transaction().context.get('process_id')
        if res_id:
            return res_id
        return None

    @staticmethod
    def order_process(tables):
        table, _ = tables[None]
        return [table.process != None, table.process]
