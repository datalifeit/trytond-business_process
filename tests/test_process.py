# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import unittest
import trytond.tests.test_tryton
from trytond.tests.test_tryton import ModuleTestCase, with_transaction
from trytond.transaction import Transaction
from trytond.pool import Pool


class BusinessProcessTestCase(ModuleTestCase):
    """Test Business process module"""
    module = 'business_process'

    @with_transaction()
    def test_create_process(self):
        """Create a process"""
        Process = Pool().get('business.process')

        Process.create([{'name': 'Process 1'}])


def suite():
    suite = trytond.tests.test_tryton.suite()
    suite.addTests(unittest.TestLoader().loadTestsFromTestCase(BusinessProcessTestCase))
    return suite
